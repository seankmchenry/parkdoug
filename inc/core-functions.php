<?php
/**
 * Core theme functions
 *
 * @package _s
 */

/**
 * Flush rewrite rules on theme activation
 */
function _s_flush_rewrite_rules() {
  flush_rewrite_rules();
}
add_action( 'after_switch_theme', '_s_flush_rewrite_rules' );

/**
 * Google analytics setup
 */
function _s_google_analytics() { ?>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', '<?php echo get_theme_mod( 'analytics_id' ); ?>', 'auto');
    ga('send', 'pageview');
  </script>
<?php }
if ( get_theme_mod( 'analytics_id' ) ) {
  add_action( 'wp_head', '_s_google_analytics' );
}

/**
 * Clean up header area
 *
 * @link http://gomakethings.com/remove-junk-from-the-wordpress-header
 */
function _s_clean_up_header() {
  remove_action( 'wp_head', 'rsd_link' );
  remove_action( 'wp_head', 'wlwmanifest_link' );
  remove_action( 'wp_head', 'wp_generator' );
  remove_action( 'wp_head', 'feed_links_extra', 3 );
  remove_action( 'wp_head', 'start_post_rel_link' );
  remove_action( 'wp_head', 'index_rel_link' );
  remove_action( 'wp_head', 'adjacent_posts_rel_link' );
  remove_action( 'wp_head', 'rel_canonical', 10, 0 );
  remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
}
add_action( 'init', '_s_clean_up_header' );

/**
 * Add custom image sizes
 *
 * @link http://codex.wordpress.org/Function_Reference/add_image_size
 */
function _s_custom_image_sizes() {
  // large size
  update_option( 'large_size_w', 950 );
  update_option( 'large_size_h', 950 );
  update_option( 'large_crop', 0 );

  // medium size
  update_option( 'medium_size_w', 650 );
  update_option( 'medium_size_h', 650 );
  update_option( 'medium_crop', 0 );
}
add_action( 'init', '_s_custom_image_sizes' );

/**
 * Add ACF options pages
 */
if ( function_exists( 'acf_add_options_page' ) ) {
  acf_add_options_page();
}

/*
Hide ACF fields dashboard
 */
function _s_hide_acf_dashboard() {
  // check if this is my user account
  if ( wp_get_current_user()->user_login === 'sean' ) {
    return;
  }
  define( 'ACF_LITE' , true );
}
add_action( 'admin_init', '_s_hide_acf_dashboard' );

/**
 * Yoast SEO meta box priority
 */
function _s_move_yoast_seo_meta() {
  return 'low';
}
add_filter( 'wpseo_metabox_prio', '_s_move_yoast_seo_meta' );

/**
 * Add SVG support and fix ACF thumbnail sizes
 */
function _s_add_svg_media_upload( $mimes ) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
function _s_fix_svg_thumbnails() {
  echo '<style>
    .acf-input img[src$=".svg"] {
      max-width: 150px;
      max-height: 150px;
    }
  </style>';
}
add_filter( 'upload_mimes', '_s_add_svg_media_upload' );
add_action( 'admin_head', '_s_fix_svg_thumbnails' );

/**
 * Get featured image URL
 */
function _s_get_feat_img_url( $size ) {
  // hat tip: http://goo.gl/fzHOaB
  $img_id = get_post_thumbnail_id();
  $img_array = wp_get_attachment_image_src( $img_id, $size );
  $img_url = $img_array[0];
  return $img_url;
}

/**
 * Get home page ID
 */
function _s_get_home_ID() {
  $home_id = get_option( 'page_on_front' );
  return $home_id;
}

/**
 * Get raw phone number
 */
function _s_get_phone_link( $phone ) {
  $phone = preg_replace( "/[^0-9,.]/", "", $phone );
  $link = "tel:" . $phone;
  return $link;
}

/**
 * Get initials from name
 */
function _s_get_initials( $name ) {
  $words = explode( " ", $name );
  foreach ( $words as $word ) {
    $letters .= $word{0};
  }
  return strtoupper( $letters );
}

/**
 * Show all posts on tax archives
 */
function _s_tax_archive_post_num( $query ) {
  if ( !is_admin() && $query->is_main_query() ) {
    if( is_tax() ) {
      $query->set( 'posts_per_page', -1 );
    }
  }
}
add_action( 'pre_get_posts', '_s_tax_archive_post_num' );
