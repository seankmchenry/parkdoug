<?php
/**
 * _s functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _s
 */

/**
 * Composer autoload
 */
require_once( __DIR__ . '/packages/autoload.php' );

/**
 * Core theme setup
 */
require get_template_directory() . '/inc/core-setup.php';

/**
 * Core theme functions
 */
require get_template_directory() . '/inc/core-functions.php';

/**
 * Custom template tags
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Extra theme functions
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer functions/fields
 */
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/utility/customizer-fields.php'; // remove me

/**
 * CPT Class
 */
require get_template_directory() . '/inc/utility/cpt-setup.php';

/**
 * TGM Plugin Activation
 */
require get_template_directory() . '/inc/utility/tgm-setup.php';

/**
 * Advanced Custom Fields
 */
require get_template_directory() . '/inc/utility/acf-fields.php'; // remove me
